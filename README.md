# ajaxDemo
js的ajaxdemo 包含后台PHP响应部分，请部署在php环境下运行

同步提交按钮会以表单的形式将数据提交到http://localhost/Syncres.php  接口上
异步ajax提交按钮会将数据提交到http://localhost/ajaxres.php 接口上

ajax异步请求的使用场景一般是用来局部刷新页面，比如网页中加载一列图文列表，移动端的上拉加载，在页面不跳转的情况下加载新的内容到页面中。
