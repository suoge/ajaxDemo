<?php
// 设置内容编码格式，避免中文乱码
header("Content-type: text/html; charset=utf-8");
// 接收前端传来的参数
$name = $_POST['name'];
$age = $_POST['age'];
// 模拟网络传输的耗时，5秒后再返回json给前端
sleep(5);
// 将前端传来的数据以json格式返回给前端
echo json_encode(array('msg'=>'成功','data'=> array('name' =>$name , 'age'=>$age)));
 ?>
